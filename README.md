Square Me

There is just one rule. Find one not matching square and tap it! Prepare for wasting a lot of time!



Project Setup


Requirement
Devkit


Install Project

```
#!javascript

git clone https://bitbucket.org/incredibl3/square-me.git
```


At your project dir

```
#!javascript

devkit install

```

Run Project

```
#!javascript

devkit serve
```


Open browser and visit: http://localhost:9200/