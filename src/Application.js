import device;
import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;
import src.Config as config;
import src.MainMenuScreen as MainMenuScreen;
import src.GameScreen as GameScreen;
import src.EndGameScreen as EndGameScreen;
import ui.StackView as StackView;

var BG_WIDTH = config.bgWidth;
var BG_HEIGHT = config.bgHeight;
var app;
exports = Class(GC.Application, function () {

  this.initUI = function () {

    app = this;

    this.setScreenDimensions(BG_WIDTH > BG_HEIGHT);

    GLOBAL.screenWidth = config.bgWidth;
    GLOBAL.screenHeight = config.bgHeight;

    var mainMenuScreen = new MainMenuScreen();
    var gameScreen = new GameScreen();
    var endScreen = new EndGameScreen();


    var rootView = new StackView({
      superview: this.view,
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      scale: 1,
      clip: true,
      zIndex: 1,
      backgroundColor: "white"
    });

    rootView.push(mainMenuScreen);
    // rootView.push(endScreen);

    /* Listen for an event dispatched by the title screen when
     * the start button has been pressed
     */
    mainMenuScreen.on('MainMenuScreen:play', function(){
      rootView.push(gameScreen);
      gameScreen.startGame();
    });

    /* When the game screen has signalled that the game is over,
     * show the end screen so that the user may play the game again.
     */
    gameScreen.on('GameScreen:end', function (score) {
      rootView.push(endScreen);
      endScreen.updateScore(score);
    });

    gameScreen.on('GameScreen:goHome', function () {
      rootView.pop();
    });

    endScreen.on('EndGameScreen:play', function () {
      console.log("hungnt end game play");
      rootView.pop();
      gameScreen.startGame();
    });

  };

  this.launchUI = function () {

  };

  /**
   * setScreenDimensions
   * ~ normalizes the game's root view to fit any device screen
   */
  this.setScreenDimensions = function(horz) {
    var ds = device.screen;
    var vs = this.view.style;
    vs.width = horz ? ds.width * (BG_HEIGHT / ds.height) : BG_WIDTH;
    vs.height = horz ? BG_HEIGHT : ds.height * (BG_WIDTH / ds.width);
    vs.scale = horz ? ds.height / BG_HEIGHT : ds.width / BG_WIDTH;
  };

});