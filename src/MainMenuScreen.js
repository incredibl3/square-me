import device;

import ui.TextView as TextView;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.widget.GridView as GridView;
import src.Config as config;
import src.lib.uiInflater as uiInflater;

//## Class: Application
exports = Class(ImageView, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight,
      image: "resources/images/mainmenuBg.png"
    });

    supr(this, 'init', [opts]);
    console.log("hungnt main menu screen");

    var playBtn = new ImageView({
      parent: this,
      layout: 'box',
      centerX: true,
      y: 750,
      width: 80,
      height: 80,
      image: "resources/images/playButton.png"
    });

    playBtn.on("InputSelect", bind(this, function () {
      console.log("hungnt mm input");
      this.emit("MainMenuScreen:play");
    }));
  };
});