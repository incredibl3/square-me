import device;
import AudioManager;
import ui.TextView as TextView;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.widget.GridView as GridView;
import ui.widget.ButtonView as ButtonView;

import math.util as util;
import ui.Color as Color;
import src.Config.GameScreenConfig as config;
import src.lib.uiInflater as uiInflater;

var app;
var BG_WIDTH;
var BG_HEIGHT;
var timer;
var interval;
//## Class: Application
exports = Class(ImageView, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT
    });
    this._sound = new AudioManager({
      path: "resources/sounds/",
      files: {
        bad: {
          volume: 1,
          loop: false
        },
        pop: {
          volume: 1,
          loop: false
        }
      }
    });
    BG_WIDTH = GLOBAL.screenWidth;
    BG_HEIGHT = GLOBAL.screenHeight;

    app = this;

    supr(this, 'init', [opts]);
    this.scoreView = new View(merge({parent: this, x: 0, y: BG_WIDTH + 150}, config.ScoreLayer));
    uiInflater.addChildren(config.ScoreLayer.children, this.scoreView);

    new ImageView({
      parent: this,
      layout: 'box',
      x: 2,
      y: GLOBAL.screenHeight - 30,
      width: GLOBAL.screenWidth,
      height: 20,
      image: "resources/images/bottom.png"
    });

    this.scoreView.pauseBtn.on("InputStart", bind(this, function(){
      this.pause();
    }));
  };

  this.pause = function(){
    console.log("hungnt pause");
    clearInterval(interval);

    var pauseBg = new View({
      parent: this,
      x: 0,
      y: 0,
      width: BG_WIDTH,
      height: BG_HEIGHT,
      backgroundColor: "black",
      opacity: 0.3
    });
    var pausePanel = new View({
      parent: this,
      layout: 'box',
      centerX: true,
      centerY: true,
      width: BG_WIDTH - 100,
      height: 400,
      backgroundColor: "white",
    });

    var restart = new ImageView({
      superview: pausePanel,
      image: "resources/images/restart.png",
      x: 30,
      y: 150,
      width: 96,
      height: 96
    });

    var home = new ImageView({
      superview: pausePanel,
      image: "resources/images/home.png",
      x: 200,
      y: 150,
      width: 96,
      height: 96
    });

    var resume = new ImageView({
      superview: pausePanel,
      image: "resources/images/playButton.png",
      x: 370,
      y: 150,
      width: 96,
      height: 96
    });

    restart.on("InputStart", bind(this, function(){
      app.startGame();
      pauseBg.hide();
      pausePanel.hide();
    }));

    home.on("InputStart", bind(this, function(){
      app.emit("GameScreen:goHome");
      pauseBg.hide();
      pausePanel.hide();
    }));

    resume.on("InputStart", bind(this, function(){
      pauseBg.hide();
      pausePanel.hide();
      app.startTimer(timer);
    }));
  }

  this.startGame = function(){
    this.score = 0;
    this.timer = config.gameTime;
    app.scoreView.secondTxt.setText(timer);
    app.scoreView.scoreTxt.setText(this.score);
    this.squareNumber = config.startSquareNumber;
    this.generateSquare(this.squareNumber);
    this.startTimer(this.timer);
  };

  this.generateSquare = function(number){

    this._gridView = new GridView({
      parent: this,
      backgroundColor: "white",
      layout: 'box',
      centerX: true,
      y: 150,
      width: BG_WIDTH - 50,
      height: BG_WIDTH - 50,
      cols: number,
      rows: number,
      //Make hide cells out of the grid range...
      hideOutOfRange: true,
      //Make cells in the grid range visible...
      showInRange: true
    });

    this._gridView.updateOpts({verticalGutter: 5});
    this._gridView.updateOpts({horizontalGutter: 5});

    var randomR = util.random(0, 255);
    var randomG = util.random(0, 255);
    var randomB = util.random(0, 255);

    var color = new Color(randomR, randomG, randomB, 1);
    var colorDif = new Color(randomR + config.colorGap, randomG + config.colorGap, randomB + config.colorGap, 1);

    var random = util.random(0, number * number - 1);
    var i = 0;
    for (var x = 0; x < number; x++) {
      for (var y = 0; y < number; y++) {
        if(i == random)
        {
          console.log("hungnt differentSquare is: [" + x + ";" + y + "]");
          var differentSquare = new View({superview: this._gridView, backgroundColor: colorDif.toString(), row: x, col: y});
          differentSquare.once('InputStart', function(){
            app._sound.play("pop");
            app._gridView.removeAllSubviews();
            app.squareNumber++;
            app.score++;
            app.squareNumber = app.squareNumber <= config.maxSquare ? app.squareNumber : config.maxSquare;
            app.scoreView.scoreTxt.setText(app.score );
            app.generateSquare(app.squareNumber);
          });
        }else{
          var commonSquare = new View({superview: this._gridView, backgroundColor: color.toString(), row: x, col: y});
          commonSquare.once('InputStart', function(){
            app._sound.play("bad");
          });
        }
        i++;
      }
    }

    this._gridView.reflow();
  }

  this.startTimer = function(duration){
    timer = duration;
    interval = setInterval(function () {

        app.scoreView.secondTxt.setText(timer);

        if (--timer == 0) {
            app.scoreView.secondTxt.setText(duration);
            app.endGame();
            clearInterval(interval);
        }
    }, 1000);
  }

  this.endGame = function(){
    app.emit("GameScreen:end", app.score);
  }

});