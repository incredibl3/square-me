import device;

import ui.TextView as TextView;
import ui.View as View;
import ui.ImageView as ImageView;
import ui.widget.GridView as GridView;
import src.Config as config;
import src.lib.uiInflater as uiInflater;
import ui.Color as Color;
//## Class: Application
exports = Class(ImageView, function (supr) {
  this.init = function (opts) {
    opts = merge(opts, {
      x: 0,
      y: 0,
      width: GLOBAL.screenWidth,
      height: GLOBAL.screenHeight
    });

    supr(this, 'init', [opts]);
    console.log("hungnt end screen");

    this._gridView = new GridView({
      parent: this,
      backgroundColor: "white",
      layout: 'box',
      centerX: true,
      y: 100,
      width: GLOBAL.screenWidth - 50,
      height: GLOBAL.screenHeight - 500,
      cols: 1,
      rows: 3,
      //Make hide cells out of the grid range...
      hideOutOfRange: true,
      //Make cells in the grid range visible...
      showInRange: true
    });

    this._gridView.updateOpts({verticalGutter: 5});

    var label = new View({superview: this._gridView, backgroundColor: '#f2c545', row: 0, col: 0});
    var labelTxt = new TextView({
      parent: label,
      layout: 'box',
      centerX: true,
      centerY: true,
      text: "square me",
      width: 400,
      height: 100,
      size: 40,
      color: "white"
    });

    var score = new View({superview: this._gridView, backgroundColor: '#e07d28', row: 1, col: 0});
    this.scoreTxt = new TextView({
      parent: score,
      layout: 'box',
      centerX: true,
      centerY: true,
      text: "score: 1",
      width: 400,
      height: 100,
      size: 40,
      color: "white"
    })
    var medal = new View({superview: this._gridView, backgroundColor: '#c17953', row: 2, col: 0});
    var medalIcon = new ImageView({
      parent: medal,
      layout: 'box',
      centerX: true,
      centerY: true,
      width: 70,
      height: 70,
      image: "resources/images/medal.png"
    });

    var playBtn = new ImageView({
      parent: this,
      layout: 'box',
      centerX: true,
      y: 750,
      width: 70,
      height: 70,
      image: "resources/images/playButton.png"
    });

    playBtn.on("InputStart", bind(this, function () {
      console.log("hungnt end game input");
      this.emit("EndGameScreen:play");
    }));


    new ImageView({
      parent: this,
      layout: 'box',
      x: 2,
      y: GLOBAL.screenHeight - 30,
      width: GLOBAL.screenWidth,
      height: 20,
      image: "resources/images/bottom.png"
    });
  };

  this.updateScore = function(score){
    this.scoreTxt.setText("score: " + score);
  };
});