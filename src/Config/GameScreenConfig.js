import device;

var BG_WIDTH = 576;
var BG_HEIGHT = 1024;

exports = {
  bgWidth: BG_WIDTH,
  bgHeight: BG_HEIGHT,
  startSquareNumber: 2,
  maxSquare: 7,
  colorGap: 15,
  gameTime: 60,
  ScoreLayer: {
    name: 'ScoreLayer',
    x: 0,
    y: 0,
    width: BG_WIDTH,
    height: 200,
    direction: "vertical",
    children: [
      {
        cls: "ui.TextView",
        name: "secondTxt",
        text: '60',
        color: 'grey',
        x: 30,
        y: true,
        width: BG_WIDTH / 3,
        height: 200,
        size: 60,
        fontFamily: 'Tahoma',
        zIndex: 1
      },
      {
        cls: "ui.TextView",
        name: "secondLabel",
        text: 'sec',
        color: 'grey',
        x: 30,
        y: 50,
        width: BG_WIDTH / 3,
        height: 200,
        size: 30,
        fontFamily: 'Tahoma',
        zIndex: 1
      },
      {
        cls: "ui.ImageView",
          name: "pauseBtn",
          layout: "box",
          centerX : true,
          y: 70,
          width: 128,
          height: 80,
          zIndex: 5,
          image: "resources/images/pause.png",
      },
      {
        cls: "ui.TextView",
        name: "scoreTxt",
        text: '0',
        color: 'grey',
        x: 380,
        centerY: true,
        width: BG_WIDTH / 3,
        height: 200,
        size: 60,
        fontFamily: 'Tahoma',
        zIndex: 1
      },
      {
        cls: "ui.TextView",
        name: "scoreLabel",
        text: 'score',
        color: 'grey',
        x: 380,
        y: 50,
        width: BG_WIDTH / 3,
        height: 200,
        size: 30,
        fontFamily: 'Tahoma',
        zIndex: 1
      }
  ]
  }
};